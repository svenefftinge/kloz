const { NODE_ENV } = process.env;
const API_URL = process.env.VUE_APP_API_URL;

export default {
  NODE_ENV,
  API_URL,
};
