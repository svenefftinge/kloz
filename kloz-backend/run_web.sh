#!/bin/sh

# prepare init migration
python manage.py makemigrations

# migrate db, so we have the latest db schema
python manage.py migrate

# collect all static files in one directory
python manage.py collectstatic --noinput

# Run the uswgi server
uwsgi --show-config
