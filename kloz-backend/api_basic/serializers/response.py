from rest_framework import serializers
from ..models import Response


class ResponseSerializer(serializers.ModelSerializer):
  id = serializers.ReadOnlyField()
  created_at = serializers.ReadOnlyField()

  class Meta:
    model = Response
    fields = '__all__'
