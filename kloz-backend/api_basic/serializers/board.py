from rest_framework import serializers
from ..models import Board


class BoardSerializer(serializers.ModelSerializer):
  id = serializers.ReadOnlyField()
  created_at = serializers.ReadOnlyField()

  class Meta:
    model = Board
    fields = '__all__'
