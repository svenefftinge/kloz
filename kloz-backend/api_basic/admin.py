from django.contrib import admin
from .models import User, Company, Board, Subject, Response, Vote

# Register your models here.
admin.site.register(User)
admin.site.register(Company)
admin.site.register(Board)
admin.site.register(Subject)
admin.site.register(Response)
admin.site.register(Vote)
